package com.example.weckerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import static com.example.weckerapp.SettingsActivity.KEY_SNOOZE_TIME;

public class AlarmActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String KEY_ALARM_ACTIVE = "is_alarm_active";
    private static final String DEFAULT_SNOOZE_TIME = "1";
    private int snooze_time;
    public static SharedPreferences prefs;
    Uri alert_sound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        alert_sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert_sound == null) {
            alert_sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            if (alert_sound == null) {
                alert_sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), alert_sound);
        mp.start();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String pref = prefs.getString(KEY_SNOOZE_TIME, DEFAULT_SNOOZE_TIME);
        if (pref != null) {
            snooze_time = Integer.parseInt(pref, 10);
        }
    }
}
